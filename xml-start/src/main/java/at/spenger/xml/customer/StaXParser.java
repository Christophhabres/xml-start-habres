package at.spenger.xml.customer;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Stack;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.XMLEvent;

import at.spenger.xml.customer.Customer;

public class StaXParser {

	public List<Customer> read(InputStream in) throws IOException {
		final String CUSTOMER = "Kunde";
		List<Customer> cusList = new ArrayList<>();

		// First, create a new XMLInputFactory
		XMLInputFactory inputFactory = XMLInputFactory.newInstance();

		// Setup a new eventReader
		try {
			XMLEventReader eventReader = inputFactory.createXMLEventReader(in);
			// read the XML document
			Customer item = null;
			while (eventReader.hasNext()) {
				XMLEvent ev = eventReader.nextEvent();
				if (ev.isStartElement()) {

					switch (ev.asStartElement().getName().getLocalPart()) {
					case CUSTOMER:
						// If we have an item element, we create a new item
						item = new Customer();
						break;
					case "name":
						ev = eventReader.nextEvent();
						item.setName(ev.asCharacters().getData());
						continue;
					case "telefon":
						ev = eventReader.nextEvent();
						item.setTelefon(ev.asCharacters().getData());
						continue;
					case "email":
						ev = eventReader.nextEvent();
						item.setEmail(ev.asCharacters().getData());
						continue;
					case "plz":
						ev = eventReader.nextEvent();
						item.setPlz(Integer.parseInt(ev.asCharacters().getData()));
						continue;
					case "ort":
						ev = eventReader.nextEvent();
						item.setOrt(ev.asCharacters().getData());
						continue;
					case "strasse":
						ev = eventReader.nextEvent();
						item.setStrasse(ev.asCharacters().getData());
						continue;
					case "kreditkartennr":
						ev = eventReader.nextEvent();
						item.setKredidkartennr(Integer.parseInt(ev.asCharacters().getData()));
						continue;
					case "kreditkartencvc":
						ev = eventReader.nextEvent();
						item.setKredidkartencvc(Integer.parseInt(ev.asCharacters().getData()));
						continue;
					case "track1":
						ev = eventReader.nextEvent();
						item.setTrack1(ev.asCharacters().getData());
						continue;
					case "track2":
						ev = eventReader.nextEvent();
						item.setTrack2(ev.asCharacters().getData());
						continue;
					}
				}
				if (ev.isEndElement()) {
					if (ev.asEndElement().getName().getLocalPart()
							.equals(CUSTOMER)) {
						cusList.add(item);
					}

				}
			}
		} catch (XMLStreamException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return cusList;
	}
	public void readSimple(InputStream in) throws IOException {
		// First, create a new XMLInputFactory
		XMLInputFactory inputFactory = XMLInputFactory.newInstance();

		Stack<String> stck = new Stack<String>();

		// Setup a new eventReader
		try {
			XMLEventReader eventReader = inputFactory.createXMLEventReader(in);

			while (eventReader.hasNext()) {
				XMLEvent ev = eventReader.nextEvent();
				if (ev.isStartElement()) {
					stck.push(ev.asStartElement().getName().getLocalPart());

					@SuppressWarnings("unchecked")
					Iterator<Attribute> iter = ev.asStartElement()
							.getAttributes();
					while (iter.hasNext()) {
						Attribute a = iter.next();
						System.out.println(buildXPathString(
								stck,
								"/@" + a.getName().getLocalPart() + "=\""
										+ a.getValue() + "\""));
					}
				}
				if (ev.isCharacters()) {
					String s = ev.asCharacters().getData();
					if (s.trim().length() > 0)
						System.out.println(buildXPathString(stck, "=\"" + s
								+ "\""));
				}
				if (ev.isEndElement())
					stck.pop();

			}

		} catch (XMLStreamException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private String buildXPathString(Stack<String> stck, String postfix) {
		StringBuffer sb = new StringBuffer();
		for (String s : stck)
			sb.append("/").append(s);
		sb.append(postfix);
		return sb.toString();
	}
}
	
	