
package at.spenger.xml.customer;

import javax.xml.bind.annotation.XmlElement;

public class Customer {

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getTelefon() {
		return telefon;
	}
	public void setTelefon(String telefon) {
		this.telefon = telefon;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public int getPlz() {
		return plz;
	}
	public void setPlz(int plz) {
		this.plz = plz;
	}
	public String getOrt() {
		return ort;
	}
	public void setOrt(String ort) {
		this.ort = ort;
	}
	public String getStrasse() {
		return strasse;
	}
	public void setStrasse(String strasse) {
		this.strasse = strasse;
	}
	public int getKredidkartennr() {
		return kredidkartennr;
	}
	public void setKredidkartennr(int kredidkartennr) {
		this.kredidkartennr = kredidkartennr;
	}
	public int getKredidkartencvc() {
		return kredidkartencvc;
	}
	public void setKredidkartencvc(int kredidkartencvc) {
		this.kredidkartencvc = kredidkartencvc;
	}
	public String getTrack1() {
		return track1;
	}
	public void setTrack1(String track1) {
		this.track1 = track1;
	}
	public String getTrack2() {
		return track2;
	}
	public void setTrack2(String track2) {
		this.track2 = track2;
	}
	@XmlElement(required = true)
	protected String name;
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result + kredidkartencvc;
		result = prime * result + kredidkartennr;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((ort == null) ? 0 : ort.hashCode());
		result = prime * result + plz;
		result = prime * result + ((strasse == null) ? 0 : strasse.hashCode());
		result = prime * result + ((telefon == null) ? 0 : telefon.hashCode());
		result = prime * result + ((track1 == null) ? 0 : track1.hashCode());
		result = prime * result + ((track2 == null) ? 0 : track2.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Customer other = (Customer) obj;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		if (kredidkartencvc != other.kredidkartencvc)
			return false;
		if (kredidkartennr != other.kredidkartennr)
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (ort == null) {
			if (other.ort != null)
				return false;
		} else if (!ort.equals(other.ort))
			return false;
		if (plz != other.plz)
			return false;
		if (strasse == null) {
			if (other.strasse != null)
				return false;
		} else if (!strasse.equals(other.strasse))
			return false;
		if (telefon == null) {
			if (other.telefon != null)
				return false;
		} else if (!telefon.equals(other.telefon))
			return false;
		if (track1 == null) {
			if (other.track1 != null)
				return false;
		} else if (!track1.equals(other.track1))
			return false;
		if (track2 == null) {
			if (other.track2 != null)
				return false;
		} else if (!track2.equals(other.track2))
			return false;
		return true;
	}
	@XmlElement(required = true)
	protected String telefon;
	@Override
	public String toString() {
		return "Customer [name=" + name + ", telefon=" + telefon + ", email="
				+ email + ", plz=" + plz + ", ort=" + ort + ", strasse="
				+ strasse + ", kredidkartennr=" + kredidkartennr
				+ ", kredidkartencvc=" + kredidkartencvc + ", track1=" + track1
				+ ", track2=" + track2 + "]";
	}
	@XmlElement(required = true)
	protected String email;
	@XmlElement(required = true)
	protected int plz;
	@XmlElement(required = true)
	protected String ort;
	@XmlElement(required = true)
	protected String strasse;
	@XmlElement(required = true)
	protected int kredidkartennr;
	@XmlElement(required = true)
	protected int kredidkartencvc;
	@XmlElement(required = true)
	protected String track1;
	@XmlElement(required = true)
	protected String track2;

}

