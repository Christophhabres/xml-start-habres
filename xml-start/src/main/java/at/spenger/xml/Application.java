package at.spenger.xml;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.annotation.Import;
import org.springframework.core.io.ClassPathResource;

import at.spenger.xml.customer.Customer;
import at.spenger.xml.customer.StaXParser;
import at.spenger.xml.xpath.XPathReader;

@Import(DefaultConfig.class)
public class Application implements CommandLineRunner {

	public static void main(String[] args) {
		 new SpringApplicationBuilder(Application.class)
		    .showBanner(false)
		    .logStartupInfo(false)
		    .run(args);
	}

	@Override
	public void run(String... arg0) throws Exception {
		readShiporder();
		XPathReader xPathReader = new XPathReader();
		xPathReader.readOrfFeed();
	}

	private void readShiporder() throws IOException {
		InputStream in = new ClassPathResource("cust.xml")
				.getInputStream();
		StaXParser p = new StaXParser();

		List<Customer> items = p.read(in);
		System.out.println("----- XML Ausgabe -----");
		for (Customer item : items) {
			System.out.println(item+" "+ item.getName());
		}
	}
}
