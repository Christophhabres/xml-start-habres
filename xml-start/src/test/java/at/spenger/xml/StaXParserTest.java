package at.spenger.xml;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import at.spenger.xml.customer.Customer;
import at.spenger.xml.customer.StaXParser;
@ActiveProfiles("test")
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = DefaultConfig.class)
public class StaXParserTest {

	private StaXParser parser;
	
	@Before
	public void setUp() throws Exception {
		parser = new StaXParser();
		
	}

	@Test
	public void test() {
        try {
			InputStream in = ApplicationTests.class.getResource("test/resources/shiporder-test.xml").openStream();
			List<Customer> l = parser.read(in);
			
			assertEquals(2, l.size());
			
		} catch (IOException e) {
			assertFalse("Testdatei fehlt!", true);
		}
	}

}
